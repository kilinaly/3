﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonMove : Node
{
    [SerializeField] private Transform root;
    [SerializeField] private Animator animator;
    [SerializeField] private int speed = 1;
    private GameObject[] gameObjects;
    private GameObject skeleton;

    private void Start()
    {
        gameObjects =  GameObject.FindGameObjectsWithTag("PointOfGround");
        skeleton = GameObject.FindGameObjectWithTag("Skeleton");
    }

    public override NodeState Evaluate()
    {
        animator.SetInteger("Movement", speed);
        if (this.gameObject.transform.position.z<gameObjects[0].transform.position.z)
        {
            skeleton.transform.position = new Vector3(gameObjects[1].transform.position.x, 0f, gameObjects[1].transform.position.z); 
        }
        else
        {
            root.Translate(Time.deltaTime * speed * Vector3.back, Space.World);   
        }
        return NodeState.Success;
    }
}
