﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAttack : Node
{
    [SerializeField] private Animator animator;
    private Coroutine attackCoroutine;
    private bool isAttack = false;
    public PlayerController playerController_script;
    
    
    public override NodeState Evaluate()
    {
        if (playerController_script.health!=0)
        {
            if (attackCoroutine != null)
            {
                return NodeState.Running;
            }

            var player = FindObjectOfType<PlayerController>();
            if (player == null)
            {
                return NodeState.Failure;
            }

            if (Vector3.Distance(transform.position, player.transform.position) > 1f)
            {
                return NodeState.Failure;
            }

            if(playerController_script.health!=0) attackCoroutine = StartCoroutine(AttackProcess());
            
            if (isAttack)
            {
                AttackHit();
                isAttack = false;
            }
        
            return NodeState.Success;
            
        }
        else
        {
            return NodeState.Failure; 
        }
        
    }

    private IEnumerator AttackProcess()
    {
        animator.SetTrigger("Attack");
        yield return new WaitForSeconds(1f);
        attackCoroutine = null;
        isAttack = true;
    }

    private void AttackHit()
    {
        if (playerController_script.health>1)
        {
            playerController_script.characterState = CharacterState.Hit;
            playerController_script.Hit();  
        }
        if (playerController_script.health==1)
        {
            playerController_script.characterState = CharacterState.Dying;
            playerController_script.Dying();  
        }

        playerController_script.health--;
        print($"Player was attacked! Health: {playerController_script.health}/{playerController_script.maxHealth}");
        if (playerController_script.health==0)print($"Player is dead!");
    }
}
