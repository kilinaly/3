﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterState
{    
    Idle,
    Move,
    Attack,
    Skill,
    Hit,
    Dying,
    Dead,
    
}

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    public CharacterState characterState;
    [SerializeField] public int health=2;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform firePoint;
    [SerializeField] private Transform skillPoint;
    [SerializeField] private GameObject deadShotEffect;

    //private static readonly int NothingTrigger = Animator.StringToHash("Nothing");
    private static readonly int DeathTrigger = Animator.StringToHash("Death");
    private static readonly int HitTrigger = Animator.StringToHash("Hit");
    private static readonly int AttackTrigger = Animator.StringToHash("Attack");
    private static readonly int SkillTrigger = Animator.StringToHash("Skill");

    public int maxHealth;

    public void AttackEvent()
    {
        var obj = ObjectsPool.Instance.GetObject(ballPrefab);
        obj.transform.position = firePoint.transform.position;
        obj.transform.rotation = firePoint.transform.rotation;
        
        var rig = obj.GetComponent<Rigidbody>();
        if (rig != null)
        {
            rig.velocity = Vector3.zero;
            rig.AddForce(Vector3.forward * 5f, ForceMode.Impulse);
        }
    }
    
    private void Reset()
    {
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        characterState = CharacterState.Idle;
        InputController.OnInputAction += OnInputCommand;
        
        ObjectsPool.Instance.PrepareObject(ballPrefab, 5);
        ObjectsPool.Instance.PrepareObject(ballPrefab, 10);

        maxHealth = health;
        
        animator.SetBool("Alive", true);
    }

    private void OnDestroy()
    {
        InputController.OnInputAction -= OnInputCommand;
    }

    private void OnInputCommand(InputCommand command)
    {
        switch (command)
        {
            case InputCommand.Fire:
                Attack();
                break;
            case InputCommand.Skill:
                Skill();
                break;
            case InputCommand.DeadShot:
                DeadShot();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(command), command, null);
        }
    }
    
    
    private void Dead()
    {
        //gameObject.transform.Rotate(90,0,0);
        //animator.enabled = false;
    }
    
    public void Hit()
    {
        animator.SetTrigger(HitTrigger);

        DelayRun.Execute(delegate
        {
            characterState = CharacterState.Idle;
        }, 1f, gameObject); 
        
}
    public void Dying()
    {
        animator.SetBool("Alive", false);
        animator.SetTrigger(DeathTrigger);

        DelayRun.Execute(delegate
        {
            characterState = CharacterState.Dead;
        }, 1f, gameObject);
        Dead();
    }
    
    private void Attack()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }

        animator.SetTrigger(AttackTrigger);
        characterState = CharacterState.Attack;
        
        
    }

    private void Skill()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        
        animator.SetTrigger(SkillTrigger); 
        characterState = CharacterState.Skill;
        
        DelayRun.Execute(delegate
        {
            characterState = CharacterState.Idle;
        }, 2f, gameObject);
        
        
    }

    private void DeadShot()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        
        animator.SetTrigger(SkillTrigger);
        DelayRun.Execute(delegate { characterState = CharacterState.Idle; }, 1.65f, gameObject);
        DelayRun.Execute(DeadShotExecute, 1.35f, gameObject);
    }

    private void DeadShotExecute()
    {
        RaycastHit[] hits;
        hits = Physics.RaycastAll(skillPoint.position, skillPoint.forward, 100.0F);

        if (hits == null)
        {
            return;
        }

        var obj = Instantiate(deadShotEffect, skillPoint.position, skillPoint.rotation);
        Destroy(obj, 1f);
        
        var healthObjects = new List<Health>();
        foreach (var hit in hits)
        {
            var health = hit.transform.GetComponent<Health>();
            if (health != null) 
            {
                healthObjects.Add(health);
                if (healthObjects.Count == 3)
                {
                    break;
                }
            }
        }

        var timer = 0f;
        foreach (var healthObject in healthObjects)
        {
            DelayRun.Execute(delegate { healthObject.SetDamage(int.MaxValue); }, timer, gameObject);
            timer += 0.2f;
        }
    }
}
