﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using System.IO;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] public GameObject[] GroundTypes;
    [SerializeField] public GameObject endArch;
    [SerializeField] public GameObject endPillars;
    [SerializeField] public GameObject endFireHolders;
    [SerializeField] public GameObject startPillars;
    [SerializeField] public GameObject levelStartPoint;
    [SerializeField] public GameObject leftWallCorner;
    [SerializeField] public GameObject rightWallCorner;
    [SerializeField] public GameObject[] wallA;
    [SerializeField] public GameObject[] wallB;
    
    [Header("Level settings")]
    private GameObject inst_gameobj;
    [Range(3,10)]
    public int GroundWidth=3;
    [Range(4,10)]
    public int GroundLength=4;

    private int randNumber;
    
    GameObject[] gameObjects;//массив объектов  созданного уровня, создаётся для удаления созданного уровня

    public string saveFileName = "Data/Save/Positions.txt";

    public void CreateBaseLevel()//создаёт уровень по настройкам
    {
        if (gameObjects!=null)
        {
            DeleteCreatedBaseLevel();
        }
        
        float xPos = levelStartPoint.transform.position.x;
        float zPos = levelStartPoint.transform.position.z;
        int sign;
        int jj;
        float chWall=0f;
        float chPillar=0f;
        float chCorner=1f;
        
        if ((GroundWidth % 2) == 0)
        {
            xPos += 2;
            chWall = -3f;
            chCorner = 2f;
            chPillar=0.5f;
        }
        
        for (int i = 0; i < GroundLength; i++)
        {
            for (int j = 0; j < GroundWidth; j++)
            {
                if (j<GroundWidth/2)
                {
                    sign = -1;
                    jj = j + 1;
                }
                else
                {
                    sign = 1;
                    jj = j-GroundWidth/2;
                }
                
                randNumber = Random.Range(0,4);
                inst_gameobj = Instantiate (GroundTypes[randNumber], new Vector3(xPos+jj*4*sign, 
                    levelStartPoint.transform.position.y,zPos+4*i), Quaternion.identity) as GameObject;
                inst_gameobj.tag = "objOfCreatedLevel";
            }
        }
        
        int halfGroundWidth = GroundWidth / 2;
        if (GroundWidth>4)
        {
            inst_gameobj = Instantiate (rightWallCorner, new Vector3(xPos-halfGroundWidth*4+1.2f, 
                levelStartPoint.transform.position.y,zPos+0.5f), Quaternion.identity) as GameObject;
            inst_gameobj.transform.localScale=new Vector3(-1f,1f,-1f);
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (leftWallCorner, new Vector3(-inst_gameobj.transform.position.x+chCorner, 
                levelStartPoint.transform.position.y,zPos+1f), Quaternion.identity) as GameObject;
            inst_gameobj.transform.localScale=new Vector3(-1f,1f,-1f);
            inst_gameobj.tag = "objOfCreatedLevel";
            
            inst_gameobj = Instantiate (leftWallCorner, new Vector3(xPos-halfGroundWidth*4, 
                levelStartPoint.transform.position.y,zPos+4*(GroundLength-3.2f)), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (rightWallCorner, new Vector3(-inst_gameobj.transform.position.x-1f, 
                levelStartPoint.transform.position.y,inst_gameobj.transform.position.z+1.6f), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            
        }
        
        if (GroundWidth>6)
        {

                inst_gameobj = Instantiate (wallB[0], new Vector3(xPos-halfGroundWidth*2.5f-1f, 
                levelStartPoint.transform.position.y,inst_gameobj.transform.position.z+1.2f), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (wallB[1], new Vector3(-inst_gameobj.transform.position.x-1f, 
                levelStartPoint.transform.position.y,inst_gameobj.transform.position.z), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (wallB[1], new Vector3(xPos-halfGroundWidth*2.5f-1f, 
                levelStartPoint.transform.position.y,zPos-1f), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (wallB[0], new Vector3(-inst_gameobj.transform.position.x-1f, 
                levelStartPoint.transform.position.y,zPos-1f), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";

            if (GroundWidth>7)
            {
                inst_gameobj = Instantiate (wallB[1], new Vector3(xPos-halfGroundWidth*3-1f, 
                levelStartPoint.transform.position.y,zPos+GroundLength*4-10f), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (wallB[0], new Vector3(-inst_gameobj.transform.position.x, 
                levelStartPoint.transform.position.y,inst_gameobj.transform.position.z), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (wallB[0], new Vector3(xPos-halfGroundWidth*3-1f, 
                levelStartPoint.transform.position.y,zPos-1f), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            inst_gameobj = Instantiate (wallB[1], new Vector3(-inst_gameobj.transform.position.x-1f, 
                levelStartPoint.transform.position.y,zPos-1f), Quaternion.identity) as GameObject;
            inst_gameobj.tag = "objOfCreatedLevel";
            }
        }
        
        if (GroundLength>4)
        {
            sign = 1;
            for (int j = 0; j < 2; j++)
            {
                for (int i = 0; i < GroundLength-4; i++)
            {
                randNumber = Random.Range(0,4);
                inst_gameobj = Instantiate (wallA[randNumber], new Vector3(xPos+4*halfGroundWidth*sign+chWall, 
                    levelStartPoint.transform.position.y,zPos+5f+4f*i), Quaternion.identity) as GameObject;
                inst_gameobj.transform.Rotate(0f,90f,0f);
                inst_gameobj.tag = "objOfCreatedLevel";    
            }
                sign = -1;
                chWall = 0;
            }
        }
        
        if ((GroundWidth % 2) == 0)
        {
            xPos -= 2.5f;
        }

        inst_gameobj = Instantiate(endArch, new Vector3(xPos,
            levelStartPoint.transform.position.y, zPos+(GroundLength-1)*4-4), Quaternion.identity);
        inst_gameobj.tag = "objOfCreatedLevel";
        
        inst_gameobj = Instantiate(endPillars, new Vector3(xPos+chPillar,
            levelStartPoint.transform.position.y, zPos+(GroundLength-1)*4-10), Quaternion.identity);
        inst_gameobj.tag = "objOfCreatedLevel";
        
        inst_gameobj = Instantiate(endFireHolders, new Vector3(xPos,
            levelStartPoint.transform.position.y, zPos+(GroundLength-1)*4-13), Quaternion.identity);
        inst_gameobj.tag = "objOfCreatedLevel";
        
        inst_gameobj = Instantiate(startPillars, new Vector3(xPos,
            levelStartPoint.transform.position.y, zPos+6), Quaternion.identity);
        inst_gameobj.tag = "objOfCreatedLevel";
    }
    
    public void DeleteCreatedBaseLevel()
    {
        gameObjects =  GameObject.FindGameObjectsWithTag("objOfCreatedLevel");

        foreach (var obj in gameObjects)
        {
            DestroyImmediate(obj);
        }
    }

    public void SavePosition()
    {
        gameObjects =  GameObject.FindGameObjectsWithTag("objOfCreatedLevel");
        StreamWriter file = new StreamWriter(saveFileName);
        
        file.WriteLine(GroundWidth);
        file.WriteLine(GroundLength);

        foreach (var gameObject in gameObjects)
        {
            file.WriteLine(gameObject.transform.position.x);
            file.WriteLine(gameObject.transform.position.y);
            file.WriteLine(gameObject.transform.position.z);
            
        }
        file.Close();
    }
    
    public void LoadPosition()
    {
        float x, y, z;
        int i = 0;
        gameObjects =  GameObject.FindGameObjectsWithTag("objOfCreatedLevel");
        
        StreamReader file = new StreamReader(saveFileName);
        
        if (file != null)
        {
            GroundWidth = System.Convert.ToInt32(file.ReadLine());
            GroundLength = System.Convert.ToInt32(file.ReadLine());

            while (!file.EndOfStream)
            {
                

                CreateBaseLevel();

                foreach (var gameObject in gameObjects)
                {
                    x = System.Convert.ToSingle(file.ReadLine());
                    y = System.Convert.ToSingle(file.ReadLine());
                    z = System.Convert.ToSingle(file.ReadLine());
                    if (gameObject!=null)
                    {
                        gameObject.transform.position = new Vector3(x, y, z);
                    }
                }
            }
        }
        file.Close();
    }

}