﻿using UnityEngine;
using UnityEditor;


[CustomEditor((typeof(LevelGenerator)))]
public class EditorSettingsForLevelGenerator : Editor
{   
    
    
    public override void OnInspectorGUI()
    {
    DrawDefaultInspector();
    LevelGenerator LG = (LevelGenerator) target;
    if (GUILayout.Button("Создать уровень по текущим настройкам"))
    {
        LG.CreateBaseLevel();
    }
    
    
    if (GUILayout.Button("Удалить уровень"))
    {
        LG.DeleteCreatedBaseLevel();
    }
    
    if (GUILayout.Button("Сохранить созданный уровень"))
    {
        LG.SavePosition();
    }
    
    if (GUILayout.Button("Загрузить созданный ранее уровень"))
    {
        LG.LoadPosition();
    }
    

    
    }
    
    
}
